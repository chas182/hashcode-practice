'use strict';

const fs = require('fs');

exports.execution = (line, level) => {
  const padding = new Array((level || 0) + 1).join('\t');
  fs.appendFileSync('executions.log', `${padding}${line || ''}\n`);
};

exports.solution = (taskId, solver, evaluator) => {
  fs.writeFileSync(`./solutions/${taskId}/${solver}.out`, evaluator.solution());
};
