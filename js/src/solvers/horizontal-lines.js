'use strict';

module.exports = (image, evaluator) => {
  let y = 0;
  for (const line of image.data) {
    let currentLength = 0;
    let painting = false;
    let startX = 0;
    let x = 0;
    for (const pixel of [...line, false]) {
      if (!painting && pixel) {
        painting = true;
        currentLength = 0;
        startX = x;
      } else if (painting && !pixel) {
        painting = false;
        currentLength = 0;
        evaluator.paintLine(startX, y, x - 1, y);
      }
      if (painting) {
        currentLength++;
      }
      x++;
    }
    y++;
  }
};
