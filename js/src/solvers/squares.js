'use strict';

const Solver = require('../Solver');

const fits = (image, center, size) => {
  for (let x = center[0] - size; x <= center[0] + size; x++) {
    for (let y = center[1] - size; y <= center[1] + size; y++) {
      if (!image.get(x, y)) {
        return false;
      }
    }
  }
  return true;
};

const clear = (image, center, size) => {
  for (let x = center[0] - size; x <= center[0] + size; x++) {
    for (let y = center[1] - size; y <= center[1] + size; y++) {
      image.set(x, y, false);
    }
  }
};

module.exports = (image, evaluator, taskId, level) => {
  let size = Math.min(image.width, image.height);
  let cx = 0;
  while (size > 0) {
    for (let y = size; y < image.height - size; y++) {
      for (let x = size; x < image.width - size; x++) {
        if (fits(image, [x, y], size)) {
          clear(image, [x, y], size);
          const recursed = Solver(image, `square-${size}`, null, level + 1, [
            'horizontal-lines',
            'vertical-lines',
            'squares'
          ]);
          evaluator.paintSquare(x, y, size);
          evaluator.copyCommands(recursed);
          return;
        }
      }
    }
    size--;
  }
  evaluator.cancel();
};
