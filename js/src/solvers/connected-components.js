'use strict';

const Image = require('../Image');
const Solver = require('../Solver');

const directions = [
  [0, -1],
  [0,  1],
  [-1, 0],
  [ 1, 0]
];

const getNeighbours = (image, x, y) => {
  const neighbours = [];
  for (const dir of directions) {
    let px = x + dir[0];
    let py = y + dir[1];
    if (px < 0 || px >= image.width || py < 0 || py >= image.height) {
      continue;
    }
    if (!image.get(px, py)) {
      continue;
    }
    neighbours.push([px, py]);
  }
  return neighbours;
};

module.exports = (image, api, taskId, level) => {
  const { width, height } = image;

  const solutions = [];

  let nextIndex = 1;
  const labels = Image.create(width, height);
  for (let y = 0; y < height; y++) {
    for (let x = 0; x < width; x++) {
      if (labels.get(x, y) || !image.get(x, y)) {
        continue;
      }
      labels.set(x, y, nextIndex);
      let points = [[x, y]];
      let nextPoints = [[x, y]];
      while (nextPoints.length) {
        const current = nextPoints.shift();
        const neighbours = getNeighbours(image, current[0], current[1]);
        for (const point of neighbours) {
          if (labels.get(point[0], point[1])) {
            continue;
          }
          labels.set(point[0], point[1], nextIndex);
          nextPoints.push(point);
          points.push(point);
        }
      }

      const part = Image.createWithPoints(points);
      // console.log(part.toString(), '\n');
      solutions.push(Solver(part, `cc-${nextIndex}`, null, level + 1, [
        'horizontal-lines',
        'vertical-lines',
        'squares'
      ]));

      nextIndex++;
    }
  }

  for (const solution of solutions) {
    api.copyCommands(solution);
  }
};
