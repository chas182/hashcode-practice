'use strict';

module.exports = (image, evaluator) => {
  for (let x = 0; x < image.width; x++) {
    let startY = 0;
    let painting = false;
    for (let y = 0; y <= image.height; y++) {
      const pixel = image.data[y] ? image.data[y][x] : false;
      if (painting && !pixel) {
        painting = false;
        evaluator.paintLine(x, startY, x, y - 1);
      } else if (!painting && pixel) {
        painting = true;
        startY = y;
      }
    }
  }
};
