'use strict';
const fs = require('fs');

const Image = require('./Image');

const fileName = process.argv[2];
const data = fs.readFileSync(fileName).toString();
const lines = data.split('\n');
lines.shift();

const width = process.argv[3], height = process.argv[4];
const image = Image.create(width, height, '.');

const commands = lines.map(line => line.split(' '));
for (const command of commands) {
  switch(command[0]) {
  case 'PAINT_SQUARE':
    image.rect(+command[2], +command[1], +command[3]);
    break;
  case 'PAINT_LINE':
    image.line(+command[2], +command[1], +command[4], +command[3]);
    break;
  case 'ERASE_CELL':
    image.erase(+command[2], +command[1]);
    break;
  };
}

console.log(`${height} ${width}`);
console.log(image.toString());
