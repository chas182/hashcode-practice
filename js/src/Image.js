'use strict';

const fs = require('fs');

module.exports = class Image {
  static parse(fileName) {
    const data = fs.readFileSync(fileName).toString();
    let textLines = data.split('\n');

    const config = textLines.shift();
    const [rows, columns] = config.split(' ').map(Number);

    textLines = textLines.slice(0, rows);
    const image = textLines.map(line =>
      line.split('').map(i => i === '#')
    );

    return new Image(image, columns, rows);
  }

  static create(width, height, empty) {
    const image = [];
    for (let y = 0; y < height; y++) {
      let row = [];
      for (let x = 0; x < width; x++) {
        row.push(empty || 0);
      }
      image.push(row);
    }
    return new Image(image, width, height);
  }

  static createWithPoints(points) {
    const min = [1e10, 1e10];
    const max = [0, 0];
    for (const point of points) {
      if (point[0] < min[0]) {
        min[0] = point[0];
      }
      if (point[1] < min[1]) {
        min[1] = point[1];
      }
      if (point[0] > max[0]) {
        max[0] = point[0];
      }
      if (point[1] > max[1]) {
        max[1] = point[1];
      }
    }

    const width = max[0] - min[0] + 1;
    const height = max[1] - min[1] + 1;

    const image = Image.create(width, height);
    image.offset = [min[0], min[1]];
    for (const point of points) {
      image.data[point[1] - min[1]][point[0] - min[0]] = 1;
    }

    return image;
  }

  constructor(data, width, height) {
    this.data = data;
    this.width = width;
    this.height = height;
  }

  get(x, y) {
    return this.data[y][x];
  }
  set(x, y, value) {
    this.data[y][x] = value;
  }

  rect(cx, cy, size) {
    for (let x = cx - size; x <= cx + size; x++) {
      for (let y = cy - size; y <= cy + size; y++) {
        this.set(x, y, '#');
      }
    }
  }
  line(x1, y1, x2, y2) {
    const inc = [0, 0];
    if (x1 === x2) {
      inc[1] = Math.sign(y2 - y1);
    } else if (y1 === y2) {
      inc[0] = Math.sign(x2 - x1);
    } else {
      throw new Error('Bad command found');
    }

    const current = [x1, y1];
    while (current[0] != x2 || current[1] != y2) {
      this.set(current[0], current[1], '#');
      current[0] += inc[0];
      current[1] += inc[1];
    }
    this.set(current[0], current[1], '#');
  }
  erase(x, y) {
    this.set(x, y, '.');
  }

  toString() {
    return this.data.map(line => line.join('')).join('\n');
  }
};
