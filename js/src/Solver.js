'use strict';
const fs = require('fs');
const path = require('path');

const Evaluator = require('./Evaluator');
const Logger = require('./Logger');
const Image = require('./Image');

module.exports = (image, name, taskId, level, solvers) => {
  Logger.execution(`Starting session ${name}`, level);

  let bestSolution = null;

  for (const solverFile of solvers) {
    const solver = require(`./solvers/${solverFile}`);
    const evaluator = new Evaluator(`${name}:${solverFile}`, image.offset);

    evaluator.start();
    solver(image, evaluator, taskId, level);
    evaluator.stop();

    Logger.execution([
      new Date(),
      solverFile,
      evaluator.elapsed().toFixed(3) + 'ms',
      evaluator.fitness()
    ].join('\t'), level);

    if (taskId) {
      Logger.solution(taskId, solverFile, evaluator);
    }

    if (bestSolution === null || evaluator.fitness() < bestSolution.fitness()) {
      bestSolution = evaluator;
    }
  }

  Logger.execution();
  return bestSolution;
};
