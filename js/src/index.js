'use strict';
const fs = require('fs');
const path = require('path');

// Helper classes
const Image = require('./Image');
const Solver = require('./Solver');
const Logger = require('./Logger');

// Read input params
const fileName = process.argv[2];
const taskId = path.basename(fileName);

// Create data dirs
const taskDir = `./solutions/${taskId}`;
if (!fs.existsSync(taskDir)) {
  fs.mkdirSync(taskDir);
}

// Start root solver
Logger.execution(`Processing root task for ${fileName}`);
fs.writeFileSync('./solution.out', Solver(Image.parse(fileName), 'root', taskId, 1, [
  'horizontal-lines',
  'vertical-lines',
  'connected-components'
]));
