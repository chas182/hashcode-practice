'use strict';

const execTimer = require('exectimer');
const Tick = execTimer.Tick;

module.exports = class Evaluator {
  static combine(solutions, name, offset) {
    const evaluator = new Evaluator(name, offset);
    for (const solution of solutions) {
      evaluator.commands.push.apply(evaluator.commands, solution.commands);
    }
    return evaluator;
  }

  constructor(name, offset) {
    this.name = name;
    this.commands = [];
    this.timer = new Tick(name);
    this.offset = offset || [0, 0];
  }

  start() {
    this.timer.start();
  }
  stop() {
    this.timer.stop();
  }

  paintLine(x1, y1, x2, y2) {
    this.commands.push([
      'PAINT_LINE',
      y1 + this.offset[1],
      x1 + this.offset[0],
      y2 + this.offset[1],
      x2 + this.offset[0]
    ]);
  }
  paintSquare(x, y, radius) {
    this.commands.push([
      'PAINT_SQUARE',
      y + this.offset[1],
      x + this.offset[0],
      radius
    ]);
  }
  eraseCell(x, y) {
    this.commands.push([
      'ERASE_CELL',
      y + this.offset[1],
      x + this.offset[0]
    ]);
  }
  copyCommands(evaluator) {
    this.commands.push.apply(this.commands, evaluator.commands);
  }

  cancel() {
    this.cancelled = true;
  }
  fitness() {
    if (this.cancelled) {
      return 1e10;
    }
    return this.commands.length;
  }
  elapsed() {
    return execTimer.timers[this.name].duration() / 1000 / 1000;
  }

  solution() {
    let solution = `${this.commands.length}\n`;
    solution += this.commands.map(command =>
      command.join(' ')
    ).join('\n');
    return solution;
  }

  toString() {
    return this.solution();
  }
}
