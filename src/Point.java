/**
 * Created by TDiva on 2/7/16.
 */

    // x y -----
    // |
    // |
public class Point {
    public int x;
    public int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Point left() {
        if (y == 0) {
            return null;
        }
        return new Point(x,y-1);
    }

    public Point right(int MAX) {
        if (y >= MAX-1) {
            return null;
        }
        return new Point(x,y+1);
    }

    public Point up() {
        if (x == 0) {
            return null;
        }
        return new Point(x-1,y);
    }

    public Point down(int MAX) {
        if (x >= MAX -1 ) {
            return null;
        }
        return new Point(x+1,y);
    }

}
