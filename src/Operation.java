import java.util.Arrays;

import static java.util.stream.Collectors.joining;

/**
 * Created by TDiva on 2/7/16.
 */
public class Operation {

    private OperationType type;
    private int[] params;

    public Operation(OperationType type, int[] params) {
        this.type = type;
        this.params = params;
    }

    public OperationType getType() {
        return type;
    }

    public int[] getParams() {
        return params;
    }

    public static Operation drawSquare(int x, int y, int r) {
        int[] params = {x,y,r};
        return new Operation(OperationType.PAINT_SQUARE, params);
    }

    public static Operation drawVerticalLine(int x1, int x2, int y) {
        int[] params = {x1, y, x2, y};
        return new Operation(OperationType.PAINT_LINE, params);
    }

    public static Operation drawHorizontalLine(int x, int y1, int y2) {
        int[] params = {x, y1, x, y2};
        return new Operation(OperationType.PAINT_LINE, params);
    }

    public static Operation eraseCell(int x, int y) {
        int[] params = {x, y};
        return new Operation(OperationType.ERASE_CELL, params);
    }

    public void apply(boolean[][] input) {
        switch (type) {
            case PAINT_LINE:  // {x1, y1, x2, y2}
                for (int i = params[0]; i <= params[2]; ++i)
                    for (int j = params[1]; j <= params[3]; ++j)
                        input[i][j] = true;
                break;
            case PAINT_SQUARE:  // {x, y, r}
                for (int i = params[0] - params[2]; i <= params[0] + params[2]; ++i)
                    for (int j = params[1] - params[2]; j <= params[1] + params[2]; ++j)
                        input[i][j] = true;
                break;
            case ERASE_CELL:  // {x, y}
                    input[params[0]][params[1]] = false;
                break;
        }
    }

    @Override
    public String toString() {
        return type.name() + " " + Arrays.stream(params).mapToObj(i -> String.valueOf(i)).collect(joining(" "));
    }

    public enum OperationType {
        PAINT_SQUARE,
        PAINT_LINE,
        ERASE_CELL;
    }
}
