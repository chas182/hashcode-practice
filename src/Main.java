import java.util.ArrayList;
import java.util.List;

public class Main {

    public static final String FILE_IN = "learn_and_teach.in";
    public static final String FILE_OUT = "learn_and_teach.out";

//    public static final String FILE_IN = "right_angle.in";
//    public static final String FILE_OUT = "right_angle.out";

//    public static final String FILE_IN = "logo.in";
//    public static final String FILE_OUT = "logo.out";


//    public static final String FILE_IN = "test.in";
//    public static final String FILE_OUT = "test.out";


    public static void main(String[] args) {
        boolean[][] source = InOutUtil.readImage(FILE_IN);
        for (boolean[] aSource : source) {
            printRow(aSource);
        }
        List<Operation> res = TanushaSolver.solve(source);
        InOutUtil.saveSolutionToFIle(FILE_OUT, res);

        System.out.println();

        boolean[][] test = new boolean[source.length][source[0].length];
        for (Operation op: res) {
            op.apply(test);
        }

        for (boolean[] aSource : test) {
            printRow(aSource);
        }

        System.out.print(arrayEqual(source,test));

    }

    public static void printRow(boolean[] row) {
        for (boolean b : row) {
            System.out.print(b ? '#' : ".") ;
        }
        System.out.println();
    }

    public static void testOut() {
        List<Operation> operationList = new ArrayList<>();
        operationList.add(Operation.drawSquare(0,0,10));
        operationList.add(Operation.drawVerticalLine(10,20,0));
        operationList.add(Operation.drawHorizontalLine(0, 10, 20));
        operationList.add(Operation.eraseCell(5,5));

        InOutUtil.saveSolutionToFIle(FILE_OUT, operationList);
    }

    public static boolean arrayEqual(boolean[][] ar1, boolean[][] ar2) {
        for (int i = 0; i < ar1.length; ++i) {
            for (int j = 0; j < ar2.length; ++j) {
                if (ar1[i][j] != ar2[i][j])
                    return false;
            }
        }
        return true;
    }

}
