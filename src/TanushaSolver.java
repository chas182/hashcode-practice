import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by TDiva on 2/7/16.
 */
public class TanushaSolver {

    public static List<Operation> solve(boolean[][] source) {

        int HEIGHT = source.length;
        int WIDTH = source[0].length;

        boolean[][] mark = new boolean[HEIGHT][WIDTH];

        List<Operation> operations = new ArrayList<>();
        for (int i = 0; i < HEIGHT; i++) {
            for (int j = 0; j < WIDTH; j++) {
                if (source[i][j] && !mark[i][j]) {
                    List<Operation> o = findOperation(source, mark, i, j);
                    if (o != null && !o.isEmpty()) operations.addAll(o);
                }
            }
        }
//        Collections.sort(operations, (o1,o2) -> {
//            if (o1.getType() != o2.getType()) {
//                if (o1.getType().equals(Operation.OperationType.ERASE_CELL))
//                    return 1;
//                if (o2.getType().equals(Operation.OperationType.ERASE_CELL))
//                    return -1;
//                return 0;
//            } else return 0;
//        });

        System.out.println();
        for (boolean[] aSource : mark) {
            Main.printRow(aSource);
        }
        System.out.println();
        return operations;
    }

    public static List<Operation> findOperation(boolean[][] source, boolean[][] mark, int x, int y) {
        // try to find square

        List<Operation> list = new ArrayList<>();
        int HEIGHT = source.length;
        int WIDTH = source[0].length;

        Point c = new Point(x, y);

        Point p = c;
        while (p != null && pointAt(source, p)) {
            p = p.right(WIDTH);
        }
        int right = p == null ? WIDTH - 1 : p.y -1;

        p = c;
        while (p != null && pointAt(source, p)) {
            p = p.down(HEIGHT);
        }
        int down = p == null ? HEIGHT - 1 : p.x -1;

        int len = Math.min(right - c.y, down - c.x);
        len = (len - 1) / 2;
        // try apply square
        while (true) {
            if (len <= 0)
                break;

            // check how many to erase for square
            int erase = 0;
            for (int i = x; i <= x + 2 * len; i++) {
                for (int j = y; j <= y + 2 * len; j++) {
                    erase += source[i][j] ? 0 : 1;
                }
            }

            if (erase > (len * 2 + 1)) {
                len--;
            } else break;
        }

        // draw square, erase left
        if (len > 0) {
            list.add(Operation.drawSquare(x + len, y + len, len));
            markCells(Operation.drawSquare(x + len, y + len, len), mark);

            for (int i = x; i <= x + 2 * len; i++) {
                for (int j = y; j <= y + 2 * len; j++) {
                    if (!source[i][j] && mark[i][j]) {
                        list.add(Operation.eraseCell(i, j));
                        markCells(Operation.eraseCell(i, j), mark);
                    }
                }
            }
        } else {
            int horiz = right - c.y;
            int vert = down - c.x;

            if (horiz > vert && horiz > 0) {
                list.add(Operation.drawHorizontalLine(c.x, c.y, right));
                markCells(Operation.drawHorizontalLine(c.x, c.y, right), mark);
            } else if (vert > 0) {
                list.add(Operation.drawVerticalLine(c.x, down, c.y));
                markCells(Operation.drawVerticalLine(c.x, down, c.y), mark);
            } else {
                list.add(Operation.drawSquare(c.x, c.y, 0));
                markCells(Operation.drawSquare(c.x, c.y, 0), mark);
            }
        }


        return list;

    }

    public static boolean pointAt(boolean[][] source, Point p) {
        return source[p.x][p.y];
    }

    public static void markCells(Operation o, boolean[][] mark) {
        int[] params;
        switch (o.getType()) {
            case PAINT_SQUARE:
                params = o.getParams();
                int cx = params[0];
                int cy = params[1];
                int r = params[2];

                for (int i = cx - r; i <= cx + r; i++) {
                    for (int j = cy - r; j <= cy + r; j++) {
                        mark[i][j] = true;
                    }
                }
                break;
            case PAINT_LINE:
                params = o.getParams();
                int x1 = params[0];
                int y1 = params[1];
                int x2 = params[2];
                int y2 = params[3];
                for (int i = x1; i <= x2; i++) {
                    for (int j = y1; j <= y2; j++) {
                        mark[i][j] = true;
                    }
                }
                break;
            case ERASE_CELL:
                params = o.getParams();
                int x = params[0];
                int y = params[1];
                mark[x][y] = false;
                break;

        }
    }


}
