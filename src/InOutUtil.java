import java.io.*;
import java.util.List;
import java.util.StringTokenizer;
import java.util.stream.Collectors;

/**
 * Created by TDiva on 2/7/16.
 */
public class InOutUtil {


    public static boolean[][] readImage(String fileIn) {
        try (BufferedReader in = new BufferedReader(new FileReader(fileIn))) {

            StringTokenizer ftkn = new StringTokenizer(in.readLine());
            int HEIGHT = Integer.valueOf(ftkn.nextToken());
            int WIDTH = Integer.valueOf(ftkn.nextToken());

            boolean[][] source = new boolean[HEIGHT][WIDTH];

            for (int i = 0; i < HEIGHT; i++) {
                String line = in.readLine();
                for (int j = 0; j < WIDTH; j++) {
                    source[i][j] = line.charAt(j) == '#' ? true : false;
                }
            }

            return source;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void saveSolutionToFIle(String fileName, List<Operation> res) {
        try {
            BufferedWriter out = new BufferedWriter(new FileWriter(fileName));
            out.write(res.size() + "\n");
            out.write(res.stream().map(o -> o.toString()).collect(Collectors.joining("\n")));
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
